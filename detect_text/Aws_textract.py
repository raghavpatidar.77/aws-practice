import boto3
def detect_text(bucket_name,file_name):
    client=boto3.client('textract')
    response = client.detect_document_text(
        Document={
            'S3Object': {
                'Bucket': bucket_name,
                'Name': file_name
                } 
            }
    )
    return response

#detect_text("sampleinput2","sampleText2.png")