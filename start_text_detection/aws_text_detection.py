import boto3
import time
def Invoke_text_detection(bucket_name,file_name):
    client=boto3.client('textract')

    response = client.start_document_text_detection(
        DocumentLocation={
            'S3Object': {
                'Bucket': bucket_name,
                'Name': file_name
                
            }
        }
    )
    
    JobID=(response["JobId"])
    
    return JobID
    
def checkJobStatus(JobID,max_retries,frequency):
    i=0
    client=boto3.client('textract')
    response=client.get_document_text_detection(JobId=JobID )
    status="IN_PROGRESS"
    #print("Job Status : {}".format(status),"frequency :",frequency,"max_retry :",max_retries)
    while (i<max_retries):
        i+=1
        time.sleep(frequency)
        response=client.get_document_text_detection(JobId=JobID)
        status=response["JobStatus"]
        if status=="SUCCEEDED":
            break
        
        #print("Job Status : {}".format(status))
    return status

def getResults(JobID,max_retries,frequency):
    Job_status=(checkJobStatus(JobID,max_retries,frequency))
    if Job_status=="SUCCEEDED": 
        client=boto3.client('textract')
        response=client.get_document_text_detection(JobId=JobID)
        #print(response)
        return response
    else:
        return Job_status