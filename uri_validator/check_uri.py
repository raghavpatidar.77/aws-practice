from copy import Error
from botocore.client import ClientError
import boto3
import botocore


def check_bucket(bucket_name):
    try:
        client=boto3.resource("s3")
        client.meta.client.head_bucket(Bucket=bucket_name)
        return True
    
    except Exception as e:
        return ["File/Bucket "+str(e.response['Error']['Message']),str(e.response['ResponseMetadata']['HTTPStatusCode'])]
       
        #error= "Bucket does not exist "
        
        #return [error,404]
        
#print(check_bucket("sampleinput2/"))

def check_file(bucket_name,file_name):
    s3 = boto3.resource('s3')
    try:
        s3.Object(bucket_name,file_name).load()
        return True
        '''
        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == "404":
                # The object does not exist.
                return ["File does not exist, response code :"+str(e.response['Error']['Code']),str(e.response['ResponseMetadata']['HTTPStatusCode'])]
            elif e.response['Error']['Code'] == 'InternalError':
                return ["File does not exist, response code :"+str(e.response['Error']['Code']),str(e.response['ResponseMetadata']['HTTPStatusCode'])]
            
        except botocore.exceptions.AccessDeniedException as e:
            return [str(e.response['Error']['Code']),str(e.response['ResponseMetadata']['HTTPStatusCode'])]'''
    except Exception as e:
        return ["File/Bucket "+str(e.response['Error']['Message']),str(e.response['ResponseMetadata']['HTTPStatusCode'])]
        #return ["Unknown Error \n",400]
    
#print(check_file("sampleinput2","Programming,ADA Using Python.pdf"))