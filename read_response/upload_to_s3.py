import smart_open
import datetime as dt
from read_response import read_output as ro

list_d=[]
result={}

file_names=[]


def upload(data,bucket_name,text_flag):
    
    ct=dt.datetime.now()
    file_name=get_file_name().split(".")
    output_file_path='s3://{bucket}/{timest}/{name}.json'.format(bucket=bucket_name,timest=ct.timestamp(),name=file_name[0])
    #print("="*100,"\nResult : ",str(ro.read(data,flags)))
    #print(text_flag)
    
    if text_flag=="True":
        
        #print("="*100,"\nResult : ",str(ro.get_text(data)))
        with smart_open.smart_open(output_file_path, 'w') as fout:
            reads=ro.get_text(data)
            fout.write(str(reads))
            
            
    else:
        
        #print(data)
        with smart_open.smart_open(output_file_path, 'w') as fout:
            fout.write(str(data))
        
    return output_file_path

def set_file_name(name):
    file_names.append(name)

        
def get_file_name():
    if len(file_names)>0:
        return file_names.pop()
    else:
        return "default_name"
    



        

   
#upload(["hello",{"text":"vakue"}],"sample")