from jsonschema import Draft4Validator as d4v
import json
import os

#C:\Users\user\Flask App Project\AWS-practice\schema_validator\schema.json
def validate(check):
    with open(r"{path}\schema_validator\schema.json".format(path=os.getcwd())) as f:
        schema=json.load(f)
        validator=d4v(schema)
        valid=list(validator.iter_errors(check))
        #print(list(validator.iter_errors(check,schema)))
        if valid ==[]:
            return True
        else:
            return valid
        

def validate_get(check):
    with open(r"{path}\schema_validator\get_schema.json".format(path=os.getcwd())) as f:
        schema=json.load(f)
        validator=d4v(schema)
        valid=list(validator.iter_errors(check))
        #print(list(validator.iter_errors(check,schema)))
        if valid ==[]:
            return True
        else:
            return valid
        
def validate_sync(check):
    with open(r"{path}\schema_validator\sync_schema.json".format(path=os.getcwd())) as f:
        schema=json.load(f)
        validator=d4v(schema)
        valid=list(validator.iter_errors(check))
        #print(list(validator.iter_errors(check,schema)))
        if valid ==[]:
            return True
        else:
            return valid




list_={
    "Data": {
        "s3_file_uri":"s3://sampleinput2/handwritten_note.jpg",
        "destination_uri":"s3://sync-output-files",
        "include_ocr_text": "True",
        "include_ocr_confidence": "False",
        "include_ocr_boundingbox": "True",
        "create_file_with_text_only":"False",
        "vendor_name": "aws"
        }
}


#print(validate_sync(list_))
    



    